
The Waffle bugfix release 1.7.1 is now available.

What is new in this release:
  - cmake: pass deps' cflags to the build
  - gbm: fix crash when platform lacks modifier support
  - gitlab-ci: stabilise CI runs under X
  - gitlab-ci: add more warnings, make all warnings fatal
  - gitlab-ci: update to bullseye
  - meson: add override_* support, when using waffle as submodule
  - meson: skip installing bash completion when custom prefix is used
  - meson: silence deprecation warnings
  - meson: generate cmake files only on Windows
  - meson: find wayland.xml from wayland-scanner.pc
  - misc: zsh completion
  - misc: fix dozens of compiler warnings
  - misc: update website references
  - wayland: fix build against wayland 1.20

The source is available at:

  https://waffle.freedesktop.org/files/release/waffle-1.7.1/waffle-1.7.1.tar.xz

You can also get the current source directly from the git
repository. See https://waffle.freedesktop.org for details.

----------------------------------------------------------------

Changes since 1.7.0:

Alyssa Ross (1):
      meson: find wayland.xml from wayland-scanner.pc

Dylan Baker (1):
      meson: use override_dependency and override_find_program

Emil Velikov (25):
      doc: Add release notes for 1.7.0
      wflinfo: add zsh completion
      gitlab-ci: use xvfb -noreset
      wayland: silence waffle 1.7 deprecation warnings
      third_party/threads: initialize abs_time
      tests/gl_basic_test: silence maybe-uninitialized warn
      waffle: consistently set WAFFLE_API_VERSION
      waffle: use same version API across programs
      core: use Wpointer-arith safe container_of
      core/tests: correct function type
      wflinfo: fixup sign-compare warnings
      tests/gl_basic_test: silence implicit-fallthrough warn
      tests/gl_basic_test: silence sign-compare warning
      meson: enable all the warnings
      gitlab-ci: flip the error on any warning
      tree: update website references
      doc: update references to new git location
      tree: update maintainer Chad -> Emil
      man: meson: add all dependencies
      meson: omit bash completion, with custom prefix
      gitlab-ci: update container to bullseye
      meson: use get_variable() update to meson 0.51
      meson: remove always true meson.version >= 0.46
      meson: generate cmake files only on Windows
      waffle: Bump version to 1.7.1

Jason Ekstrand (1):
      wgbm: Don't destroy a surface if init fails

Jose Fonseca (1):
      gitlab-ci: Build and package with MinGW cross-compilers.

Philipp Zabel (1):
      wayland: fix build against version 1.20

Romain Naour (1):
      cmake: forward cflags from *.pc files to waffle cflags

Simon McVittie (9):
      waffle_dl: Fix missing format string parameter
      waffle_window_create2: Fix mismatch between format string and parameter
      Mark printf-like functions with the appropriate gcc attribute
      meson: Enable format-string-related compiler warnings
      wflinfo: Print absence of GL context flags correctly
      wflinfo: Make context_flags more const-correct
      meson: Detect non-const-correct uses of string constants
      wflinfo: Move context_flags array into constant data section
      examples, wflinfo: Add missing NORETURN attributes

